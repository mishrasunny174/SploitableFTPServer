CC=gcc
CFLAGS = -no-pie -fno-stack-protector -z norelro -fno-pic

SploitableFTPServer: SploitableFTPServer.c
	$(CC) $^ -o $@ $(CFLAGS)
clean: SploitableFTPServer
	rm  $^
